# HW4 Test Scenarios
# I thought I'll share the test scenarios early so you know what functionality you have to focus on. I might add more tests but you can expect these scenarios to be there in the test script. 

 

# Test Scenario 1:

# Node additions/deletions. A kvs consists of 2 partitions with 2 replicas each. I add 3 new nodes. The number of partitions should become 4. Then I delete a node.The number of partitions should become 3. I then delete 2 more nodes. Now the number of partitions should be back to 2.

 

# Test Scenario 2:

# A kvs consists of 2 partitions with 2 replicas each. I send x number of randomly generated keys to the kvs. I add 2 nodes to the kvs. No keys should be dropped. Then, I kill a node and send a view_change request to remove the faulty instance. Again, no keys should be dropped.

 

# Test Scenario 3:

# A kvs consists of 2 partitions with 2 replicas each. I send x number of randomly generated keys to the kvs. I remove 1 partition (2 nodes) and check whether any keys were dropped. (This test is different from previous one)

 

# Test Scenario 4:

# Basic functionality for obtaining information about partitions; tests the following GET requests: 
# get_all_partitions_ids, get_partition_memebrs and get_partition_id.

 

# Test Scenario 5:

# A kvs consists of 2 partitions with 2 replicas each. I send x number of randomly generated keys to the kvs I choose 2 keys from the same partition, then I send concurrently updates to these keys. No errors should occur. Then I sleep for few seconds, update each key and verify that the updates are successful.

 

# Test Scenario 6:            

# A very simple test to verify that after we disconnect/connect a node, the kvs works as it is supposed to. The kvs consists of one node only.

 

# Test Scenario 7:

# A test with a network partition. I send x number of randomly generated key(s) to the kvs. I disconnect a node and update a key from that partition, then I connect the node back, wait for 5 seconds and disconnect the other node in partition. I verify that the key is updated. There will be only one partition in this scenario. 

 

# Test Scenario 8:

# I send x number of randomly generated key(s) to the kvs. I read a key from a node. The I update the key on another node from the same partition, providing the causal payload from the first read. Then I write a new value to the key on another node. I read from the 3rd node in the partition and verify that the value is fresh. There will be only one partition in this scenario. 

#  