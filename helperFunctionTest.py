#Testing helper functions

#Check for concurrent causal payloads:
def create_causal_payload(number_of_nodes):
    causal_payload = [0]
    for x in range(1, number_of_nodes):
        causal_payload.append(0)
    return causal_payload

'''GIVEN: two causaul payloads and corresponding times RETURN: true if happens before'''
# def happens_before(causal_payload_1,time1,nodeip_1,causal_payload_2,time2,nodeip_2):
def happens_before(causal_payload_1,time1,causal_payload_2,time2):

    #print 'event 1 vector clock: '+str(causal_payload_1.split('.'))+' event 1 time: '+str(time1)
    #print 'event 2 vector clock: '+str(causal_payload_2.split('.'))+' event 2 time: '+str(time2)
    #print hash(nodeip_1)
    #print hash(nodeip_2)
    
    greater = False
    less = False
    same = True
    for x,y in zip(causal_payload_1.split('.'),causal_payload_2.split('.')):
        if(x > y):
            greater = True
            same = False
        if(y > x):
            less = True
            same = False

    if(greater and less) or same:               #tie break causal payload with time 

        return True                           # RETURNS TRUE TO MAKE THE NEW CAUSAL PAYLOAD WIN
        # if(time1 == time2):               
        #     if(hash(nodeip_1)<hash(nodeip_2)):  #tie break time with hash of ip
        #         return True
        #     else: 
        #         return False
        if(time1 < time2):
            return True                         
        else:
            return False                  
    elif(less):                                 #causual payload 1 happened before causal payload 2
        return True   
    else:                                       #causual payload 2 happened before causal payload 1
        return False                       

#given causal payload string return payload string
def increment(causal_payload, index):
    incremented_causal_payload = "" 
    cp_list = convert_cp_to_ints(causal_payload) #cp_list looks something like [0,0,0,0]
    #add check if index out of bounds?

    #increment appropriate index
    cp_list[index] += 1
    convert_ints_to_str(cp_list) #cp_list now looks something like ['0', '0', '0', '0']

    #unsplit/recreate string
    return convert_str_list_to_str(cp_list)

def convert_int_list_to_str(causal_payload):
    return convert_str_list_to_str(convert_ints_to_str(causal_payload))


#given causal payload string, returns list of ints "0.0" to ['0', '0', '0', '0']
def convert_cp_to_ints(causal_payload):
    cp_list = causal_payload.split('.') 
    cp_list_length = len(cp_list)
    #convert list of strings to list of ints
    for x in range(0, cp_list_length):
        cp_list[x]=int(cp_list[x])
    return cp_list

#given [0,0,0,0], convert to ['0', '0', '0', '0']
def convert_ints_to_str(causal_payload):
    cp_list = causal_payload
    cp_list_length = len(cp_list)

    #convert list of ints back to strings
    for x in range(0, cp_list_length):
        cp_list[x]=str(cp_list[x])  
    return cp_list

#ex. given  ['0', '0', '0', '0'], return "0.0.0.0"
def convert_str_list_to_str(causal_payload):
    incremented_causal_payload = ""
    cp_list = causal_payload
    cp_list_length = len(causal_payload)    
    for x in range(0, cp_list_length):
        incremented_causal_payload += cp_list[x]
        incremented_causal_payload += "."

    incremented_causal_payload = incremented_causal_payload[:-1] #delete excess period
    return incremented_causal_payload

#given two payload strings, return max payload string
def max_payload(causal_payload_1, causal_payload_2):
    max_payload = []
    cp1 = convert_cp_to_ints(causal_payload_1)
    cp2 = convert_cp_to_ints(causal_payload_2)
    for i, (x,y) in enumerate(zip(cp1,cp2)):
        if x > y:
            max_payload.append(x)
        else:
            max_payload.append(y)
    return convert_int_list_to_str(max_payload)

if __name__ == "__main__":


    # print '^^^^^^^'
    #print convert_int_list_to_str([0,3,2,0,5])

    # print '^^^^^^^'

    causal_payload_1 = "0.0.0.0"
    
    causal_payload_2 = "3.4.1.3"
    #print "causal_payload_1 is " + causal_payload_1

    #print "now incrementing causal_payload_1"
    causal_payload_1 = increment(causal_payload_1, 3)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 1)
    causal_payload_1 = increment(causal_payload_1, 0)

    # print "causal_payload_1 is now " + causal_payload_1

    #print "causal_payload_2 is " + causal_payload_2

    #print "now incrementing causal_payload_1"
    causal_payload_2 = increment(causal_payload_2, 3)
    causal_payload_2 = increment(causal_payload_2, 1)
    causal_payload_2 = increment(causal_payload_2, 1)
    causal_payload_2 = increment(causal_payload_2, 0)

    # print "causal_payload_2 is now " + causal_payload_2

    # print "max_payload is " + max_payload(causal_payload_1,causal_payload_2)

    #thingy = [0,0,0,0]
    #for index, item in enumerate(thingy):
    #    thingy[index] += 1

    #print thingy


    time1 = 123412341235
    time2 = 123412341235
    nodeip_1 = "10.0.0.23"
    nodeip_2 = "10.0.0.22"
    # happens_before(causal_payload_1,time1,causal_payload_2,time2)