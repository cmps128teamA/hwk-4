from flask import Flask, request, jsonify
from helperFunctionTest import create_causal_payload, happens_before, convert_int_list_to_str, convert_cp_to_ints, convert_ints_to_str, convert_str_list_to_str, max_payload, increment
import requests
import os
import time
import json
import threading

app = Flask(__name__)

#----------------------------------------------------flooding function------------------------------
@app.route('/lookupKey', methods=['GET','PUT'])
def lookupKey():
    global lookup
    global partitions
    global replicas
    global node_causal_payload
    global node_time
    global ip_port
    p = get_partition_id_for_node(ip_port)
    key = request.form.get('key')
    request_causal_payload = request.form.get('causal_payload')


    if (request.method == 'GET') and (key in lookup):
        value = lookup.get(key)

        if(request_causal_payload == "-1"):
            current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
            current_time = time.time()
            vlist = [value[0], str(current_time),current_node_payload_string]                #     save time global
            node_causal_payload = convert_cp_to_ints(current_node_payload_string)
            node_time = current_time

        return (jsonify(msg="success", value=vlist[0], partition_id=p, causal_payload=current_node_payload_string, timestamp= vlist[1]), 200)

        # If greater than current cp
        if(happens_before(convert_int_list_to_str(node_causal_payload),node_time,request_causal_payload,time.time())): #Node cp,time < request cp, time
            current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
            max_payload_str = max_payload(current_node_payload_string,request_causal_payload)                   # get element-wise max return
            current_time = time.time()
            vlist = [value[0], str(current_time),max_payload_str]                #     save time global
            node_causal_payload = convert_cp_to_ints(max_payload_str)
            node_time = current_time

            return (jsonify(msg="success", value=vlist[0], partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)

        return (jsonify(msg="error", error="key found but causal_payload is invalid"), 200)

    if (request.method == 'PUT') and (key in lookup):

        # adding new key if causal payload is valid
        if(happens_before(convert_int_list_to_str(node_causal_payload),node_time,request_causal_payload,time.time())): #Node cp,time < request cp, time
            current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
            max_payload_str = max_payload(current_node_payload_string,request_causal_payload)                   # get element-wise max return
            current_time = time.time()
            vlist = [value, str(current_time),max_payload_str]                #     save time global
            node_causal_payload = convert_cp_to_ints(max_payload_str)
            node_time = current_time

            lookup.update({str(key):vlist})
            for v in replicas:
                if v != ip_port:
                    r = requests.put('http://'+ v +'/updateLookup', data = {"key":key,"value": vlist[0],"time": vlist[1], "causal_payload": vlist[2]})
            return (jsonify(msg="success", partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)

        # return error if old causal payload
        return (jsonify(msg="error", error="attempted to put but causal_payload is invalid"), 200)

    return (jsonify(msg = "error", error = "key not found", id=ip_port, lookup=str(lookup), key=str(key)), 404)

@app.route('/kvs', methods=['GET', 'PUT'])
def kvs():
    global lookup
    global partitions
    global node_time
    global node_causal_payload
    global check
    p = get_partition_id_for_node(ip_port)

    # if req is get
    if request.method == 'GET':

        key = request.args.get('key')
        request_causal_payload = str(request.args.get('causal_payload'))
        if(request_causal_payload == ""):
            request_causal_payload = "-1"

        if key in lookup:
            value = lookup[key]
            # Compare cp values: 
            if(request_causal_payload == "-1"):
                current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
                current_time = time.time()
                vlist = [value[0], str(current_time),current_node_payload_string]                #     save time global
                node_causal_payload = convert_cp_to_ints(current_node_payload_string)
                node_time = current_time

                return (jsonify(msg="success", value=vlist[0], partition_id=p, causal_payload=current_node_payload_string, timestamp= vlist[1]), 200)

            # If greater than current cp
            if(happens_before(convert_int_list_to_str(node_causal_payload),node_time,request_causal_payload,time.time())): #Node cp,time < request cp, time
                current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
                max_payload_str = max_payload(current_node_payload_string,request_causal_payload)                   # get element-wise max return
                current_time = time.time()
                vlist = [value[0], str(current_time),max_payload_str]                #     save time global
                node_causal_payload = convert_cp_to_ints(max_payload_str)
                node_time = current_time

                return (jsonify(msg="success", value=vlist[0], partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)


            return (jsonify(msg="error", error="key found but causal_payload is invalid"), 404)

        # get list of partitions
        l = list(partitions.keys())

        # for every partition
        for i, x in enumerate(l):

            # skip current nodes partition
            if x == get_partition_id_for_node(ip_port):
                continue 

            # get ip_port for first node in every partition
            first_node = partitions.get(x)[0]

            # check if other partition has key
            # CHANGE CAUSAL PAYLOAD
            try:    
                r = requests.get('http://'+str(first_node)+'/lookupKey', data={'key':key, 'causal_payload': request_causal_payload}, timeout=0.8)
                if r.status_code == 200:
                    return (jsonify(r.json()),200)
            except:
                pass


            first_node = partitions.get(x)[1]

            # check if other partition has key
            # CHANGE CAUSAL PAYLOAD
            try:    
                r = requests.get('http://'+str(first_node)+'/lookupKey', data={'key':key, 'causal_payload': request_causal_payload}, timeout=0.8)
                if r.status_code == 200:
                    return (jsonify(r.json()),200)
            except:
                continue

        list_lookup = list(lookup.keys())
        hi = key in list_lookup
        hi2 = key in lookup
        return (jsonify(msg="error", error="key not found"), 404)

    # if req is put
    if request.method == 'PUT':
        key = request.form.get('key') 
        value = request.form.get('value')
        request_causal_payload = request.form.get('causal_payload')
        if(request_causal_payload == ""):
            request_causal_payload = "-1"

        # check if key exists in current nodes lookup
        if key in lookup:
            #value list [value, time, vc]

            #get current list
            #check the new request validity

            # if good save new one

            #send to replicas

            if(happens_before(convert_int_list_to_str(node_causal_payload),node_time,request_causal_payload,time.time())): #Node cp,time < request cp, time
                current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
                max_payload_str = max_payload(current_node_payload_string,request_causal_payload)                   # get element-wise max return
                current_time = time.time()
                vlist = [value, str(current_time),max_payload_str]                #     save time global
                node_causal_payload = convert_cp_to_ints(max_payload_str)
                node_time = current_time

                # vlist = [value, str(time.time()), '0.0.0.0']
                lookup[key] = vlist
                #if check == True:
                try:
                    for v in replicas:
                        if v != ip_port:
                            r = requests.put('http://'+ v +'/updateLookup', data = {"key":key,"value": vlist[0],"time": vlist[1], "causal_payload": vlist[2]},timeout=0.8)
                except:
                    pass
                return (jsonify(msg="success", partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)

            return (jsonify(msg="error", error="attempted to put but causal_payload is invalid"), 404)

        # for every partition
        l = list(partitions.keys())

        # for every partition
        for x in l:
            # skip current nodes partition
            if x == get_partition_id_for_node(ip_port):
                continue 
            # get ip_port for first node in every partition
            first_node = partitions.get(x)[0]
            try:
                r = requests.put('http://'+first_node+'/lookupKey', data = {"key":key,"value": value, 'causal_payload': request_causal_payload},timeout=0.8)
                #update other nodes in the same partition with the new data
                if r.status_code == 200:
                    return (jsonify(r.json()),200)
            except:
                continue
            #if 400 and its a cp error


        #CHANGED TO CHECK CAUSAL PAYLOAD IS LESS CHANGED BECAUSE I GUESS IF CAUSAL PAYLOAD IS '' LET IT ADD
        # adding new key if causal payload is valid
        # if(happens_before(convert_int_list_to_str(node_causal_payload),node_time,request_causal_payload,time.time())): #Node cp,time < request cp, time
        #     current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
        #     max_payload_str = max_payload(current_node_payload_string,request_causal_payload)                   # get element-wise max return
        #     current_time = time.time()
        #     vlist = [value, str(current_time),max_payload_str]                #     save time global
        #     node_causal_payload = convert_cp_to_ints(max_payload_str)
        #     node_time = current_time

        #     lookup.update({str(key):vlist})
        #     for v in replicas:
        #         if v != ip_port:
        #             r = requests.put('http://'+ v +'/updateLookup', data = {"key":key,"value": vlist[0],"time": vlist[1], "causal_payload": vlist[2]})
        #     return (jsonify(msg="success", partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)

        # # return error if old causal payload
        # return (jsonify(msg="error", error="attempted to put but causal_payload is invalid"), 404)
        if(request_causal_payload == "-1"):

            current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
            
            current_time = time.time()
            vlist = [value, str(current_time),current_node_payload_string]                #     save time global
            node_causal_payload = convert_cp_to_ints(current_node_payload_string)
            node_time = current_time

            lookup.update({str(key):vlist})

            #if check == True:
            try:
                for v in replicas:
                    if v != ip_port:
                        r = requests.put('http://'+ v +'/updateLookup', data = {"key":key,"value": vlist[0],"time": vlist[1], "causal_payload": vlist[2]}, timeout=0.8)
            except:
                pass
            return (jsonify(msg="success", partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)


        current_node_payload_string = increment(convert_int_list_to_str(node_causal_payload),get_partition_id_for_node(ip_port))#     increment current vector clock, 
        max_payload_str = max_payload(current_node_payload_string,request_causal_payload)                   # get element-wise max return
        current_time = time.time()
        vlist = [value, str(current_time),max_payload_str]                #     save time global
        node_causal_payload = convert_cp_to_ints(max_payload_str)
        node_time = current_time

        lookup.update({str(key):vlist})
        #if check == True:
        try:
            for v in replicas:
                if v != ip_port:
                    r = requests.put('http://'+ v +'/updateLookup', data = {"key":key,"value": vlist[0],"time": vlist[1], "causal_payload": vlist[2]}, timeout=0.8)
        except:
            pass
        return (jsonify(msg="success", partition_id=p, causal_payload=vlist[2], timestamp= vlist[1]), 200)
#------------------------------updating replicas----------------------------------------------------
@app.route('/updateLookup', methods=['PUT'])
def updateLookup():
        global node_causal_payload
        global node_time
        
        key = request.form.get('key') 
        value = request.form.get('value')
        time = request.form.get('time')
        causal_payload = request.form.get('causal_payload')
        vlist = [value, time, causal_payload]
        lookup[key] = vlist   

        node_time = time
        node_causal_payload = convert_cp_to_ints(causal_payload)
        # lookup[key] = value  
        return (jsonify(msg="success"),200)
#-------------------------------------------------------add/delete functions--------------------------------
@app.route('/kvs/view_update', methods=['PUT'])
def view_update():
    global partitions
    global lookup
    global ip_port
    global replicas
    method = request.form.get("type")
    new_node_IP = request.form.get("ip_port")
    if method == "add":
        #add code goes here
        v = find_partition()
        if v is None:
            #this means we need to build a new partition and stick the new guy in it
            new_list = []
            new_list.append(new_node_IP)
            #add a new dictionary entry in partitions
            #new_partition_id = str(len(list(partitions.keys())))
            new_partition_id = len(list(partitions.keys()))
            partitions[new_partition_id]=new_list
            #this is now the new partitions dictionary, tell everyone else to update their partitions dictionaries
            test_array = []
            #update others
            for partition, nodes in zip(list(partitions.keys()), list(partitions.values())):
                for x in nodes:
                    if x != ip_port:                    
                        jsondump = json.dumps(partitions)
                        r = requests.put('http://'+ x +'/update_partitions', data = {"updated_partitions":jsondump})

                #make sure the key added is a unique value   ##ASSUMED that len(nodes) is ok because delete takes care of balancing
            #flood everyone else with new info!
            return (jsonify(msg="success", partition_id=new_partition_id, number_of_partitions = len(partitions)),200)

        else:
            #find existing partition
            #put it in it
            new_partition_id = v #assuming there has to be room somewhere for new guy
            # for partition, nodes in zip(list(partitions.keys()), list(partitions.values())):
            #     if len(nodes) < k:
            #         nodes.append(new_node_IP)
            #         new_partition_id=partition
            #     else:
            #         pass

            #update own dictionary
            partial_partition = partitions.get(new_partition_id)
            partial_partition.append(new_node_IP)


            #update others
            for partition, nodes in zip(list(partitions.keys()), list(partitions.values())):
                for x in nodes:
                    if x != ip_port:                    
                        jsondump = json.dumps(partitions)
                        r = requests.put('http://'+ x +'/update_partitions', data = {"updated_partitions":jsondump})


            curr_nodes = partitions.get(new_partition_id)


            my_keys_dump = json.dumps(lookup)
            #transfer keys AS WELL AS causal payload and other info from first index of nodes in partition id
            #if i am that node, then send the keys + info to new guy myself
            if curr_nodes[0] == ip_port:
                r = requests.put('http://'+ new_node_IP +'/get_lookup', data = {"my_keys_dump":my_keys_dump})
            #else i am not that node, so send a put to that node which will put keys into new node
            else:
                r = requests.put('http://'+curr_nodes[0]+'/transfer_keys', data = {"new_node_IP":new_node_IP})

            return jsonify(msg="success", partition_id=new_partition_id, number_of_partitions = len(partitions))
        
    elif method == "remove":
        #update own dictionary
        for partition, nodes in zip(list(partitions.keys()), list(partitions.values())):     
            for x in nodes:
                if x == new_node_IP:
                    nodes.remove(x)
                    #what if it wasn't found? corner case?


        # rebalance()
        test_str = rebalance(new_node_IP)
        #flood everyone except yourself with rebalanced dictionary

        #check for empty partitions
        for k, v in zip(list(partitions.keys()), list(partitions.values())):
            if len(v) == 0:
                del partitions[k]

        #set replicas list
        if get_partition_id_for_node(ip_port) != -1:
            replicas = partitions.get(get_partition_id_for_node(ip_port))

        #inform every other node of the change to view/partitions
        jsondump = json.dumps(partitions)
        for k, v in zip(list(partitions.keys()), list(partitions.values())):
            for nodes in v:
                if(nodes != ip_port):
                    r = requests.put('http://'+ nodes +'/update_partitions', data = {"updated_partitions":jsondump}) 


        return jsonify(msg="success", number_of_partitions = len(partitions))

def rebalance(delete_node_ip):
    global partitions
    global k
    global lookup
    global replicas
    partial_count = 0
    partial_partitions = []
    test_str=""
    #are there two partial partitions?
    for partition, nodes in zip(list(partitions.keys()), list(partitions.values())):     
        if len(nodes)<k:
            partial_count += 1
            partial_partitions.append(partition)

    if partial_count==1:
        if len(partitions[partial_partitions[0]]) == 0:
            #means the partition is empty
            #this is where you transfer keys to partition [0]
            if delete_node_ip == ip_port:

                #locate node to transfer keys
                for k, v in zip(list(partitions.keys()), list(partitions.values())):
                    if len(v) >= 2:
                       transfer_node = v[0]

                #get lookup from another partition     
                r = requests.get("http://"+transfer_node+"/send_lookup")
                lookup_1 = r.json()["lookup"]

                #merge both lookups
                for k, v in zip(list(lookup_1.keys()), list(lookup_1.values())):
                    lookup.update({k:v})

                #send my updated lookup to transfer partition
                my_lookup = json.dumps(lookup)
                part_id = get_partition_id_for_node(transfer_node)
                for x in partitions[part_id]:                    
                    r = requests.put('http://'+ x +'/get_lookup', data = {"my_keys_dump":my_lookup})

            else:
                #current node is not the one to be deleted

                #get lookup from node to be deleted     
                r = requests.get("http://"+delete_node_ip+"/send_lookup")
                lookup_1 = r.json()["lookup"]

                #merge both lookups
                for k, v in zip(list(lookup_1.keys()), list(lookup_1.values())):
                    lookup.update({k:v})

                #send current nodes updated lookup to replicas
                my_lookup = json.dumps(lookup)
                part_id = get_partition_id_for_node(ip_port)
                for x in partitions[part_id]:
                    if x != ip_port:                    
                        r = requests.put('http://'+ x +'/get_lookup', data = {"my_keys_dump":my_lookup})

                #test_str = "partial_partitions: " + str(partitions[partial_partitions[0]]) + " \nip_port: " + str(ip_port) 


    elif partial_count == 2:
        if (len(partitions[partial_partitions[0]])==(k-1)) and (len(partitions[partial_partitions[1]])==(k-1)):
            new_partition_list = partitions[partial_partitions[0]]
            old_partition_list = partitions[partial_partitions[1]]

            #am I the one moving?
            if old_partition_list[0] == ip_port:

                #get lookup from new neighbor
                r = requests.get("http://"+new_partition_list[0]+"/send_lookup")
                lookup_1 = r.json()["lookup"]

                #merge both lookups
                for k, v in zip(list(lookup_1.keys()), list(lookup_1.values())):
                    lookup.update({k:v})

                #send merged lookup to both nodes
                my_lookup = json.dumps(lookup)

                #send my updated lookup to new neighbor
                r = requests.put('http://'+ new_partition_list[0] +'/get_lookup', data = {"my_keys_dump":my_lookup})


            #if not, am I the new neighbor?
            elif ip_port==new_partition_list[0]:

                #get lookup from moving node
                r = requests.get("http://"+old_partition_list[0]+"/send_lookup")
                lookup_1 = r.json()["lookup"]

                #merge both lookups
                for k, v in zip(list(lookup_1.keys()), list(lookup_1.values())):
                    lookup.update({k:v})

                #send merged lookup to both nodes
                my_lookup = json.dumps(lookup)

                #send my updated lookup to new neighbor
                r = requests.put('http://'+ old_partition_list[0] +'/get_lookup', data = {"my_keys_dump":my_lookup})


            else: #we are neither the new neighbor nor the moving node

                #get the existing neighbor's lookup (in new list)
                r = requests.get("http://"+new_partition_list[0]+"/send_lookup")
                lookup_1 = r.json()["lookup"]

                #get the moving node's lookup
                r = requests.get("http://"+old_partition_list[0]+"/send_lookup")
                lookup_2 = r.json()["lookup"]

                #merge both lookups
                for k, v in zip(list(lookup_2.keys()), list(lookup_2.values())):
                    lookup_1.update({k:v})

                #send merged lookup to both nodes
                my_lookup = json.dumps(lookup_1)
                r = requests.put('http://'+ new_partition_list[0] +'/get_lookup', data = {"my_keys_dump":my_lookup})
                r = requests.put('http://'+ old_partition_list[0] +'/get_lookup', data = {"my_keys_dump":my_lookup})
                
            #move new guy into partition with smaller id (0 in this case)
            new_partition_list.append(old_partition_list[0])
            old_partition_list.remove(old_partition_list[0])

            #change partitions to account for rebalance
            changed_partition = get_partition_id_for_node(new_partition_list[0])
            partitions[changed_partition] = new_partition_list

    return str(test_str)

@app.route('/checkPulse', methods=['GET'])
def check_Pulse():
    return jsonify(msg='True') 

#----------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
@app.route('/send_lookup', methods=['GET'])
def send_lookup():
    global lookup
    #jsondump = json.dumps(lookup)
    return jsonify(msg="success", lookup=lookup)

@app.route('/send_lookup2', methods=['GET'])
def send_lookup2():
    global lookup
    jsondump = json.dumps(lookup)
    return jsonify(msg="success", lookup=jsondump)
#----------------------------------------------------------------------------------------
@app.route('/compare_lookup', methods=['GET'])
def compare_lookup():
    if request.method == 'GET':     
        #replica_lookup = get
        replica_lookup = { #test lookup dictionary
            "key": [
                "value", 
                "1521485035.4389741", 
                "2.0.0"
            ], 
            "key2": [
                "value2", 
                "1521485133.5385635", 
                "2.0.0"
            ], 
            "key3": [
                "value3", 
                "1521485192.8869724", 
                "3.0.0"
            ]
        }
        test_lookup=compare_lookups(replica_lookup)

    # new_lookup = str(lookup)
        #return jsonify(test_lookup)
        return jsonify(msg="success")

#get all new/updated keys from replica lookup
def compare_lookups(r_lookup):
    global lookup
    replica_lookup = r_lookup
    test = []
    my_metadata = []
    #go through replica's keys
    for key, metadata in zip(list(replica_lookup.keys()), list(replica_lookup.values())):
        test.append(key)
        #if I have the key
        if key in lookup:
            #test.append(key)
            #lookup[key] = my_metadata
            my_metadata=lookup[key]
            
            #compare keys 
            #if replica's key is newer 
            if happens_before(my_metadata[2], my_metadata[1], metadata[2], metadata[1]):
                #replace minee
                newTime= str(metadata[1])
                newCP = str(metadata[2])
                newval = str(metadata[0])
                my_metadata = [newval,newTime,newCP]
                lookup[key]=my_metadata
            #else (replica's key is older)
            else:
                pass
        
        #else (I don't have the key)
        else:
            #gimme dat shit
            lookup[key]=metadata
            
    return jsonify(msg="success")
    #return test
    #return replica_lookup


#----------------------------------------------------------------------------------------
@app.route('/update_partitions', methods=['PUT'])
def update_partitions():
    global partitions
    global replicas
    updated_partitions_dictionary = json.loads(request.form.get('updated_partitions'))
    partitions = updated_partitions_dictionary
    partitions = {int(k):v for k,v in partitions.items()}
    #set replicas list
    replicas = partitions.get(get_partition_id_for_node(ip_port))

    return jsonify(msg="success")
#-----------------------------------------------------------------------------------------------
#receive a lookup table and set it as my own
@app.route('/get_lookup', methods=['PUT'])
def get_lookup_table_and_update():
    global lookup
    new_lookup = json.loads(request.form.get('my_keys_dump'))
    lookup = new_lookup
    return jsonify(msg="success")

#transfer keys to new guy
@app.route('/transfer_keys', methods=['PUT'])
def transfer_keys():
    global lookup
    new_node_IP = request.form.get('new_node_IP')

    my_lookup = json.dumps(lookup)
    r = requests.put('http://'+ new_node_IP +'/get_lookup', data = {"my_keys_dump":my_lookup})  
    return jsonify(msg="success") 

#----------------------------------------------------------------------------------------
@app.route('/kvs/get_number_of_keys', methods=['GET'])
def get_number_of_keys():
    return jsonify(count=len(lookup)), 200

#----------------------------------------------------------------------------------------
#find a partial partition if it exists, else return None
def find_partition():
    global partitions
    global k
    for partition, nodes in zip(list(partitions.keys()), list(partitions.values())):
        if len(nodes) < k:
            #return len(nodes)
            return partition
    return None

#--------------------------------------------------------------------------------
lookup = {}
node_causal_payload =[]
newView=[]
check = None

# get env variables
k = int(os.environ.get('K'))
ip_port = os.environ.get('ip_port')
view_str = os.environ.get('VIEW')
if view_str:
    view = view_str.split(',')
else:
    view_str = newView.append(ip_port)

partitions = {}
replicas = []
node_time = 0

#-----------------------------------------------------debugging code --------------------------------------
@app.route('/throwup', methods=['GET'])
def throwup():
    # return jsonify(message="sucess", lookup)
    if request.method == 'GET': 
        return jsonify(check,lookup, ip_port,get_partition_id_for_node(ip_port), partitions, replicas)#, get_partition_id_for_node(ip_port))
#----------------------------------------------------------------------------------------------------------

#-----------------------------------------------------Helper functions-----------------------------------------------------
def makePartition():
    global partitions
    global replicas
    global view_str
    l = []
    if view_str:
        for i, x in enumerate(view):
            l.append(x)
            if((i > 0) and ((i+1)%k == 0)):
                partitions.update({int(int(i)/int(k)):l})
                del l
                l = list()
        if(len(view)%k != 0):
            last_id = int(len(view)/int(k))
            nodes_left = len(view)%k
            partitions.update({last_id:l[-nodes_left:]})
        replicas = partitions.get(get_partition_id_for_node(ip_port))

def get_partition_id_for_node(ip):
    for i, x in enumerate(list(partitions.values())):
        if(ip in x):
            return i
    return -1
#-----------------------------------------------------Server routes-----------------------------------------------------
@app.route('/kvs/get_partition_id', methods=['GET'])
def get_partition_id():
    p = get_partition_id_for_node(ip_port)
    if(p != -1):
        return jsonify(msg="success", partition_id=p)
    else:
        return jsonify(msg="error", error="node not found/valid")

@app.route('/kvs/get_all_partition_ids', methods=['GET']) 
def get_all_partition_ids():
    l = list(partitions.keys())
    return jsonify(msg="success", partition_id_list=l)

@app.route('/kvs/get_partition_members',methods=['GET'])
def return_partition_members():
    global partitions
    if request.method == 'GET': 
        partition_id = request.args.get('partition_id','')
        trial = partitions.get(int(partition_id),"NONE")
        if trial == "NONE":
            return jsonify(msg="error",error="Unknown partition id")
        else:
            return jsonify(msg="success",partition_members=trial)

#---------------------------------------------------------------------------------------------------------
def checkAlive():
    def do_check():
        global check
        global replicas
        global i
        

        while(True):
            for n in replicas:
                if n != ip_port:
                    replica_ip = n
            try:
                r = requests.get("http://"+replica_ip+"/checkPulse", timeout=0.5)
                if check == False:
                    check = True     
                    r = requests.get("http://"+replica_ip+"/send_lookup2", timeout=0.5)
                    lookup_1 = json.loads(r.json()["lookup"])

                    compare_lookups(lookup_1)
                else:
                    check = True
            except:
                check = False

            time.sleep(3)
        # while True:
        #   for n in replicas:
        #       if n != ip_port:
        #           replica_ip = n
    
        #   try:
        #       r = requests.get("http://"+replica_ip+"/checkPulse")
        #       if(r.status_code == 200):
        #           check = "server is connected"
        #   except:
        #       check = "server has not been connnected"
        #       #check = r.text()
        #   time.sleep(1)

    thread = threading.Thread(target=do_check)
    thread.start()




i = 0
if __name__ == "__main__":
    #app.run(host=IP_ENV, port=PORT_ENV)
    makePartition()
    node_causal_payload = create_causal_payload(len(list(partitions.keys())))
    node_time = 0
    checkAlive()
    app.run("0.0.0.0", port=8080)